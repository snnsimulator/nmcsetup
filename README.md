# nmcsetup

Contains everything needed to setup an environment on the intel neuromorphic cloud (nmc).

## Setup SSH Files
Make sure that you have the ssh keys that are known to intel in your ssh folder. This is typically `$HOME/.ssh` or `~/.ssh`.
Create or edit (if already existing) a file named config in the ssh folder. It should have the following lines added to it:
```
Host ssh.intel-research.net
  User=username
  IdentityFile ~/.ssh/vlab_gateway_rsa

Host *.research.intel-research.net
  HostName %h
  User=username
  ProxyCommand= ssh -W %h:%p ssh.intel-research.net
  IdentityFile ~/.ssh/vlab_ext_rsa
```
The `username` needs to be replaced with your nmc username.

## Connect to the nmc
To connect to the nmc you will need an account with intel for it's nmc. After you have received your credentials you can connect to it via ssh:
```
ssh -L 8080:localhost:8080 username@ncl-edu.research.intel-research.net
```
The `-L` flag allows you to connect port 8080 of the nmc to port 8080 of your computer. This is needed to run jupyter notebooks from the nmc.

The `username` part of the instruction needs to be replaced with your username and the intel url.
## Setup
To start the setup process, simply execute the following commands in the nmc.
```
wget https://gitlab.socsci.ru.nl/snnsimulator/nmcsetup/-/raw/master/python38/py38.sh -O py38.sh
chmod +x ./py38.sh
./py38.sh
```

## Usage
After the setup has been completed, you can use the following command to activate the environment:
```
source "$HOME/nengoloihi/miniconda/etc/profile.d/conda.sh" && conda activate nengoloihi38 
```
Keep in mind that this command needs to be repeated every time you log into the nmc.

## Example notebook
Directly after installation you can access notebooks, using this command:

```
SLURM=1 jupyter lab --no-browser --port 8080
```

You can use this to check out the example notebook in the nengoloihi folder.
